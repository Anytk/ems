import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {LoginComponent} from './components/login/login.component';
import {EquipmentComponent} from './components/equipment/equipment.component'


const routes: Routes = [
  {
    path:"",
    redirectTo:"/",
    pathMatch:"full"
  },
  {path:"home",component:HomeComponent},
  {path:"login",component:LoginComponent},
  {path:"equipment",component:EquipmentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
